# Bootstrapping two-dimensional CFTs with Virasoro symmetry

## Basic features
This Python 3 code is written in the Jupyter Notebook format. 

Part of this code evolved from the GitHub repository [bootstrap-2d-Python](https://github.com/ribault/bootstrap-2d-Python).

BibTeX entry: @misc{rng24, 
      author = {{S. Ribault, R. Nivesvivat, L. Grans-Samuelsson et al}}, 
      title = {Bootstrap\\_Virasoro: Bootstrapping two-dimensional CFTs with Virasoro symmetry}, 
      url = {https://gitlab.com/s.g.ribault/Bootstrap_Virasoro/}, 
      year = {2024}, 
      type = {code}, 
}

## Purpose
This code is 
for computing correlation functions in two-dimensional CFTs 
with Virasoro symmetry. We compute the spectrum and structure 
constants using analytic formulas, and the conformal blocks
using Zamolodchikov's recursion. Then we check crossing symmetry
numerically. This is done in CFTs like Liouville theory: non-rational,
at generic central charges. In the $O(n)$ and Potts models, the structure
constants are determined numerically.

## Notebooks
* **CFT.ipynb** -- Basic CFT: central charge, conformal dimensions, fields. 
* **Auxiliary_classes.ipynb** -- A few useful scripts.
* **Blocks.ipynb** -- Conformal blocks.
* **Correlators.ipynb** -- Three- and four-point functions in Liouville theory and other CFTs, including some tests of crossing symmetry.
* **Connectivities.ipynb** -- Computing four-point connectivities in the Potts model.
* **PottsOn.ipynb** -- Computing four-point functions in the Potts and O(n) models, and more generally in loop models.
* **Article_DMM_limit.ipynb** -- Figures and data for https://arxiv.org/abs/1909.10784 .
* **Article_logarithmic.ipynb** -- Data for https://arxiv.org/abs/2007.04190 .
* **On4pt.ipynb** -- Data for https://arxiv.org/abs/2111.01106 .
* **Potts4pt.ipynb** -- Data for https://arxiv.org/abs/2205.09349 .
* **Diagonal_fields.ipynb** -- Data for https://arxiv.org/abs/2209.09706 . 
* **Loop4pt.ipynb** -- Data for https://arxiv.org/abs/2302.08168 .
* **Loop_exact.ipynb** -- Data for https://arxiv.org/abs/2311.17558 . 
* **Eseries.ipynb** -- Fusion rules and correlation functions in E-series minimal models, for https://arxiv.org/abs/25xx.xxxxx .



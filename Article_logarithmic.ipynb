{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Four-point connectivities in the $Q$-state Potts model\n",
    "This notebook is for supporting the claims in Section 5 of the article on **Logarithmic CFT at generic central charge: from Liouville theory to the $Q$-state Potts model** by Rongvoram Nivesvivat and Sylvain Ribault, https://arxiv.org/abs/2007.04190 .\n",
    "\n",
    "## Unique solutions of two crossing symmetry equations\n",
    "We solve the crossing symmetry equations\n",
    "$$\n",
    "\\begin{align}\n",
    " & \\sum_{i\\in \\mathcal{S}^{abab}_\\text{reduced}} D_i^{abab} \\mathcal{H}_i^{(s)}(z_k) = \n",
    "\\sum_{i'\\in \\mathcal{S}^{aabb}_\\text{reduced}} D_{i'}^{aabb} \\mathcal{H}_{i'}^{(u)}(z_k)\\ ,\n",
    "\\label{suc}\n",
    "\\\\\n",
    "& \\sum_{i\\in \\mathcal{S}^{aaaa}_\\text{reduced}} D_i^{aaaa} \\left(\\mathcal{H}_i^{(s)}(z_k) - \\mathcal{H}_{i}^{(t)}(z_k)\\right) = 0\\ .\n",
    "\\label{stc}\n",
    "\\end{align}\n",
    "$$\n",
    "We show that the first equation has a unique solution by displaying the how the deviations for the structure constants $D^{abab}_i$ tend to zero as $N_\\text{max}$ increases. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import import_ipynb\n",
    "from Connectivities import *\n",
    "from IPython.display import display, Math\n",
    "\n",
    "\"\"\" Parameters. \"\"\"\n",
    "\n",
    "mp.dps = 30      # the number of digits in floating point numbers\n",
    "Ns = [16, 24, 32]      # the values of the cutoff Nmax\n",
    "beta = mp.mpc('.8', '.1')    # the value of beta\n",
    "charge = Charge('beta', beta)   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\" Solving the first crossing symmetry equation. \"\"\"\n",
    "\n",
    "connectivities = []\n",
    "for N in Ns:\n",
    "    spectrum = Spectrum(charge, N, 'abab')    # a truncated spectrum\n",
    "    conn = Connectivity(spectrum, repeat = 2)   \n",
    "    # a connectivity, computed using 2 random draws of the positions\n",
    "    connectivities.append(conn)\n",
    "\n",
    "connectivities[-1].dev_display()   # deviations for structure constants D^{abab}_i\n",
    "print('-----------------------------')\n",
    "connectivities[-1].dev_display(True)    # deviations for structure constants D^{aabb}_i"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\" Latex tables for the article: displaying deviations \n",
    "for the structure constants D^{aabb}_i. \"\"\"\n",
    "\n",
    "for conn in connectivities:\n",
    "    table = conn.table_display(True)\n",
    "    display(Math(table))\n",
    "    print(table)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\" Solving the second crossing symmetry equation, and displaying \n",
    "the deviations in Latex tables.\"\"\"\n",
    "\n",
    "connectivities2 = []\n",
    "for N in Ns:\n",
    "    print('N =', N)\n",
    "    spectrum = Spectrum(charge, N, 'aaaa')    # a truncated spectrum\n",
    "    conn = Connectivity(spectrum, repeat = 2)   \n",
    "    # a connectivity, computed using 2 random draws of the positions\n",
    "    connectivities2.append(conn)\n",
    "    display(Math(conn.table_display()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## An underdetermined crossing symmetry equation\n",
    "We now consider the equation\n",
    "$$\n",
    "\\begin{align}\n",
    " \\sum_{i\\in \\mathcal{S}^{abab}_\\text{reduced}} D_i^{abab} \\left(\\mathcal{H}_i^{(s)}(z_k) - (-1)^{\\text{Spin}(i)}\\mathcal{H}_{i}^{(t)}(z_k)\\right) = 0 \\ , \n",
    " \\label{stm}\n",
    "\\end{align}\n",
    "$$\n",
    "We find deviations that tend to zero for $D^{abab}_{(2,\\frac12)}$, but not for the structure constants $D^{abab}_{(r\\geq 4,s)}$. This is a signal that the solution is not unique. There is actually a family of subspectrums $\\mathcal{S}^{r_0}_\\text{reduced} \\subset \\mathcal{S}^{abab}_\\text{reduced}$ that lead to unique solutions of crossing symmetry. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 32\n",
    "spectrums = [Spectrum(charge, N, 'basis', r0 = 2),   # The largest subspectrum\n",
    "             Spectrum(charge, N, 'basis', r0 = 4),   # Another subspectrum\n",
    "             Spectrum(charge, N, 'basis', r0 = 6)]   # Another subspectrum\n",
    "connectivities4 = []\n",
    "for spectrum in spectrums:\n",
    "    conn = Connectivity(spectrum, repeat = 2)   \n",
    "    connectivities4.append(conn)\n",
    "    table = conn.table_display()\n",
    "    display(Math(table))\n",
    "    print(table)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analytic expressions for some ratios of structure constants\n",
    "We check analytic predictions for six ratios of structure constants.\n",
    "We find that the relative differences between the predictions and the numerical values tend to zero as $N_\\text{max}$ increases."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for N in Ns:\n",
    "    print('N =', N)\n",
    "    ratios = Potts_Ratios(beta, N)\n",
    "    print(ratios.errors)   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing the Delfino-Viti conjecture for the three-point connectivity\n",
    "We test the conjecture for one value of the central charge. We find that the relative \n",
    "difference between the conjecture and the numerical values tends to zero as $N_\\text{max}$ increases."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for N in Ns:\n",
    "    conn = Connectivity(Spectrum(charge, N, 'abab'))\n",
    "    ratio = conn.extract_cst((0, 1/2), True) / conn.extract_cst((1, 1), True)\n",
    "    # The relevant four-point structure constant, correctly normalized.\n",
    "    ana_ratio = -2*exp(conn.ansatz(1/2) - conn.ansatz())\n",
    "    # The conjectured analytic expression for the same quantity.\n",
    "    print('N =', N, ':', errors([ratio, ana_ratio]))  # The relative difference."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
